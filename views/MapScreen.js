var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var ScrollView = require("@juspay/mystique-backend").androidViews.ScrollView;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var Button = require("@juspay/mystique-backend").androidViews.Button;

var firebaseHelper = require('../utils/firebase-helpers');
const CURRENT_SCREEN = "MAP_SCREEN";

class MapScreen extends View {
	
	constructor(props, children, state) {
		console.log("WELCOME TO MAP SCREEN");
		super(props, children);
		this.state = {
	
		};
		this.setIds([
		]);
		
		// firebaseHelper.getUserSkills().then((snapshot) => {
		// 	this.state.toTeach =  snapshot.val().skills;
		// 	this.state.toLearn =  snapshot.val().skillsToLearn;
		// 	this.updateSkillsView();
		// }).catch((error) => {
		// 	console.log(error);
		// 	this.state.toTeach = [];
		// 	this.state.toLearn = [];
		// 	this.updateSkillsView();
		// });
	}

	handleStateChange = (data) => {
		this.state = data.state;
		var cmd= '';	
		switch(data.state[CURRENT_SCREEN].nextAction) {
			case "SHOW_NEXT_SCREEN" :
				this.props.showScreen("SIGNIN_SCREEN",{});
				console.log("SIGNIN_SCREEN");
				break;
			default :
        		throw new Error("Next action not handled "+ data.state.nextAction);
		}	
		return {runInUI : cmd};
	}

	handleBackPress = () => {
    	this.props.showScreen("SHOW_PREVIOUS_SCREEN", {});
  	}

	render() {
		this.layout = (
			<LinearLayout 
				width="match_parent"
				height="match_parent"
				gravity="center_horizontal"
				orientation="vertical"
				padding="15,15,15,15" >
				
				<ScrollView
					width="match_parent"
					height="match_parent">
					<LinearLayout
						width="match_parent"
						height="wrap_content"
						orientation="vertical">
                        
					</LinearLayout>
				</ScrollView>
			</LinearLayout>
		)	
		return this.layout.render();
	}
}
module.exports = Connector(MapScreen);