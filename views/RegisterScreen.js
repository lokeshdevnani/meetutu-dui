var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var ScrollView = require("@juspay/mystique-backend").androidViews.ScrollView;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var Button = require("@juspay/mystique-backend").androidViews.Button;
var EditText = require("@juspay/mystique-backend").androidViews.EditText;
var ProgressBar = require("@juspay/mystique-backend").androidViews.ProgressBar;

var firebaseHelper = require('../utils/firebase-helpers');
const CURRENT_SCREEN = "REGISTER_SCREEN";

class RegisterScreen extends View {
	
	constructor(props, children, state) {
		console.log("WELCOME TO SECOND SCREEN");
		super(props, children);
		this.state = {
			name: '',
			username : '',
			password : ''	
		};
		this.setIds([
			'name',
			'username',
			'password',
			'done'
		]);
	}

	handleStateChange = (data) => {
		this.state = data.state;
		var cmd= '';	
		switch(data.state[CURRENT_SCREEN].nextAction) {
			case "SHOW_NEXT_SCREEN" :
				this.props.showScreen("SIGNIN_SCREEN",{});
				console.log("SIGNIN_SCREEN");
				break;
			default :
        		throw new Error("Next action not handled "+ data.state.nextAction);
		}	
		return {runInUI : cmd};
	}

	handleBackPress = () => {
    	this.props.showScreen("SHOW_PREVIOUS_SCREEN", {});
  	}

	register = () => {
		this.setProgressBar("visible");
		firebaseHelper.register(this.state).then(()=>{
			this.props.showScreen("SIGNIN_SCREEN",{});
		}).catch((error) => {
			this.setProgressBar("gone");
			console.log(error.message);
		})
	}

	setProgressBar(status) {
		Android.runInUI(this.set({
			id: this.idSet.done,
			visibility: status
		}),null);
	}
	
	goToLogin = () => {
		this.props.showScreen("SIGNIN_SCREEN",{});
	}

	render() {
		this.layout = (
			<LinearLayout 
				width="match_parent"
				height="match_parent"
				gravity="center_horizontal"
				orientation="vertical"
				padding="15,15,15,15" >
				
				<ScrollView
					width="match_parent"
					height="match_parent">

					<LinearLayout
						width="match_parent"
						height="wrap_content"
						orientation="vertical">

							<EditText
								id={this.idSet.name}
								width="match_parent"
								height="wrap_content"
								hint="Name"
								onChange={(name)=>this.state.name=name}
								maxLines="1" />
							<EditText
								id={this.idSet.username}
								width="match_parent"
								height="wrap_content"
								hint="Email"
								onChange={(email)=>this.state.username=email}
								maxLines="1"  />
							<EditText
								width="match_parent"
								height="wrap_content"
								hint="Password"
								inputType="password"
								onChange={(pwd)=>this.state.password=pwd}
								maxLines="1"  />
							
							<ProgressBar
								id={this.idSet.done}
								width="match_parent"
								height="wrap_content"
								margin="10,10,10,10"
								visibility="gone"
								centerInParent="true,1"/>
							<Button
								width="match_parent"
								height="wrap_content"
								onClick={this.register}
								text="Register"
								background="#3F51B5"
								color="#EEEEEE"
								margin="0,10,0,0" />

							<TextView
								text="Already have an account? Login here"
								margin="40,20,10,10" />
							<Button
								width="match_parent"
								height="wrap_content"
								onClick={this.goToLogin}
								text="Back to Login" />
					</LinearLayout>
				</ScrollView>
			</LinearLayout>
		)	
		return this.layout.render();
	}
}

module.exports = Connector(RegisterScreen);