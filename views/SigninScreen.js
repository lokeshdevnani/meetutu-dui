var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var ScrollView = require("@juspay/mystique-backend").androidViews.ScrollView;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var Button = require("@juspay/mystique-backend").androidViews.Button;
var EditText = require("@juspay/mystique-backend").androidViews.EditText;
var ProgressBar = require("@juspay/mystique-backend").androidViews.ProgressBar;

var firebaseHelper = require('../utils/firebase-helpers');

const CURRENT_SCREEN = "SIGNIN_SCREEN";

class SigninScreen extends View {
	
	constructor(props, children, state) {
		super(props, children);
		this.state = {
			username : '',
			password : ''
		};
		this.setIds([
			'username',
			'password',
			'done'
		]);
	}	

	handleStateChange = (data) => {
		this.state = data.state;
		var cmd= '';
		switch(data.state[CURRENT_SCREEN].nextAction) {
			case "SHOW_NEXT_SCREEN" :
				this.props.showScreen("SHOW_REGISTER_SCREEN",{});
				console.log("SHOW_REGISTER_SCREEN");
				break;
			default :
			throw new Error("Next action not handled "+ data.state.nextAction);
			break;
		}	
		if(cmd){
			return {runInUI : cmd}
		}
	}

	setProgressBar(status) {
		Android.runInUI(this.set({
			id: this.idSet.done,
			visibility: status
		}),null);
	}

	login = () => {
		this.setProgressBar("visible");
		firebaseHelper
			.login(this.state)
			.then(()=>{
				this.props.showScreen("SHOW_LIST_SCREEN",{});
			}).catch((error) => {
				console.log(error.message);
				this.setProgressBar("gone");
			}); 
	}

	goToSignup = () => {
		// this.props.showScreen("SHOW_LIST_SCREEN",{}); return;
		this.props.showScreen("SHOW_REGISTER_SCREEN",{});
	}

	render() {
		this.layout = (
			<LinearLayout 
				width="match_parent"
				height="match_parent"
				gravity="center_horizontal"
				orientation="vertical"
				padding="15,15,15,15" >
				<ScrollView
					width="match_parent"
					height="match_parent">
					<LinearLayout
						width="match_parent"
						height="wrap_content"
						orientation="vertical">
						<EditText
							id={this.idSet.username}
							width="match_parent"
							height="wrap_content"
							hint="Email"
							onChange={(email)=>this.state.username=email}
							maxLines="1"  />
						<EditText
							width="match_parent"
							height="wrap_content"
							hint="Password"
							inputType="password"
							onChange={(pwd)=>this.state.password=pwd}
							maxLines="1"  />
						<ProgressBar
							id={this.idSet.done}
							width="match_parent"
							height="wrap_content"
							margin="10,10,10,10"
							visibility="gone"
							centerInParent="true,1"/>
						<Button
							width="match_parent"
							height="wrap_content"
							onClick={this.login}
							background="#3F51B5"
							color="#EEEEEE"
							text="Sign In"
							margin="0,10,0,0" />
						<TextView
							text="Don't have an account? Register here"
							margin="40,20,10,10" />
						<Button
							width="match_parent"
							height="wrap_content"
							onClick={this.goToSignup}
							text="Register" />
					</LinearLayout>
				</ScrollView>
			</LinearLayout>
		)	
		return this.layout.render();
	}
}

module.exports = Connector(SigninScreen);

// <ProgressBar
	// 				style="?attr/progressBarStyleLarge"
	// 				width="wrap_content"
	// 				height="wrap_content"
	// 				marginBottom="8dp"
	// 				visibility="gone" />