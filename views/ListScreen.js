var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var ScrollView = require("@juspay/mystique-backend").androidViews.ScrollView;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var Button = require("@juspay/mystique-backend").androidViews.Button;
var EditText = require("@juspay/mystique-backend").androidViews.EditText;

var firebaseHelper = require('../utils/firebase-helpers');
const CURRENT_SCREEN = "LIST_SCREEN";

class ListScreen extends View {
	
	constructor(props, children, state) {
		console.log("WELCOME TO LIST SCREEN");
		super(props, children);
		this.state = {
			toTeach: [],
			toLearn: [],
			skill: '',
			skillToLearn: '',
			skillsConcat: 'Loading...',
			skillsToLearnConcat: 'Loading...'
		};
		this.setIds([
			'skillsView',
			'skillsInput',
			'skillsToLearnView',
			'skillsToLearnInput'
		]);
		
		firebaseHelper.getUserSkills().then((snapshot) => {
			this.state.toTeach =  snapshot.val().skills;
			this.state.toLearn =  snapshot.val().skillsToLearn;
			this.updateSkillsView();
		}).catch((error) => {
			console.log(error);
			this.state.toTeach = [];
			this.state.toLearn = [];
			this.updateSkillsView();
		});
	}

	handleStateChange = (data) => {
		this.state = data.state;
		var cmd= '';	
		switch(data.state[CURRENT_SCREEN].nextAction) {
			case "SHOW_NEXT_SCREEN" :
				this.props.showScreen("SIGNIN_SCREEN",{});
				console.log("SIGNIN_SCREEN");
				break;
			default :
        		throw new Error("Next action not handled "+ data.state.nextAction);
		}	
		return {runInUI : cmd};
	}

	handleBackPress = () => {
    	this.props.showScreen("SHOW_PREVIOUS_SCREEN", {});
  	}

	updateSkillsView = () => {
		this.state.skillsConcat = this.state.toTeach.join(", ");
		Android.runInUI(this.set({
			id: this.idSet.skillsView,
			text: this.state.skillsConcat
		}), null);

		this.state.skillsToLearnConcat = this.state.toLearn.join(", ");
		Android.runInUI(this.set({
			id: this.idSet.skillsToLearnView,
			text: this.state.skillsToLearnConcat
		}), null);
	}

	addSkill = () => {
		console.log("New skill added");
		this.state.skill = this.state.skill.trim();
		if(this.state.skill=="") {
			return;
		}
		this.state.toTeach.push(this.state.skill);
		this.updateSkillsView();
		this.state.skill = '';
	
		Android.runInUI(this.set({
			id: this.idSet.skillsInput,
			text: this.state.skill
		}), null);

		firebaseHelper.updateUserSkills(this.state.toTeach, this.state.toLearn);
	}

	addSkillToLearn = () => {
		console.log("New skill to learn added");
		this.state.skillToLearn = this.state.skillToLearn.trim();
		if(this.state.skillToLearn=="") {
			return;
		}
		this.state.toLearn.push(this.state.skillToLearn);
		this.updateSkillsView();
		this.state.skillToLearn = '';
	
		Android.runInUI(this.set({
			id: this.idSet.skillsToLearnInput,
			text: this.state.skillToLearn
		}), null);

		firebaseHelper.updateUserSkills(this.state.toTeach, this.state.toLearn);
	}

	goToMap = () => {
		this.props.showScreen("SHOW_MAP_SCREEN",{
			skills: this.state.toTeach
		});
	}

	render() {
		this.layout = (
			<LinearLayout 
				width="match_parent"
				height="match_parent"
				gravity="center_horizontal"
				orientation="vertical"
				padding="15,15,15,15" >
				
				<ScrollView
					width="match_parent"
					height="match_parent">
					<LinearLayout
						width="match_parent"
						height="wrap_content"
						orientation="vertical">
                            <TextView 
                                text="​The joy meeting an expert teacher, for the craving learner. Add your skills and skills you want to learn to get started."
                                width="match_parent"
                                padding="10,10,10,10"
                                height="wrap_content" />
							<TextView
								width="match_parent"
								height="wrap_content"
                                padding="5,5,5,5"
								text="YOUR SKILLS:"
								fontSize="0,5"
								color="#000000" />
							<TextView
								id={this.idSet.skillsView} 
								width="match_parent"
								height="wrap_content"
                                padding="16,16,16,16"
								fontSize="1,3"
								text={this.state.skillsConcat} />
							<EditText
								id={this.idSet.skillsInput}
								width="match_parent"
								height="wrap_content"
								hint="Skills eg. Javascript"
								text={this.state.skill}
								onChange={(skill)=>this.state.skill=skill}
								maxLines="1"  />
							<Button
								width="match_parent"
								height="wrap_content"
								onClick={this.addSkill}
								text="Add Skill" />
							
							<TextView 
								width="match_parent"
								height="1"
								background="#AAAAAA"
								margin="0,40,0,40"
							/>

							<TextView
								width="match_parent"
								height="wrap_content"
                                padding="5,5,5,5"
								text="SKILLS YOU WANT TO LEARN:"
								fontSize="0,5"
								color="#000000" />
							<TextView
								id={this.idSet.skillsToLearnView} 
								width="match_parent"
								height="wrap_content"
                                padding="16,16,16,16"
								fontSize="1,3"
								text={this.state.skillsToLearnConcat} />
							<EditText
								id={this.idSet.skillsToLearnInput}
								width="match_parent"
								height="wrap_content"
								hint="Skills eg. Algorithms"
								text={this.state.skillToLearn}
								onChange={(skill)=>this.state.skillToLearn=skill}
								maxLines="1"  />
							<Button
								width="match_parent"
								height="wrap_content"
								onClick={this.addSkillToLearn}
								text="Add Skill" />

							<Button
								width="match_parent"
								height="wrap_content"
								onClick={this.goToMap}
								background="#3F51B5"
								color="#EEEEEE"
								margin="0,40,0,0"
								text="Meetup People" />
					</LinearLayout>
				</ScrollView>
			</LinearLayout>
		)	
		return this.layout.render();
	}
}
module.exports = Connector(ListScreen);