//Logger
//const logger = require("../utils/logger");

//Reducer
const ScreenReducer = require("../state_machines/Screens");
const SigninScreenReducer = require("../state_machines/SigninState");
const RegisterScreenReducer = require("../state_machines/RegisterScreenState");
const ListScreenReducer = require("../state_machines/ListState");
const MapScreenReducer = require("../state_machines/MapState");


const reducer = require("@juspay/mystique-backend").stateManagers.reducer({
	"SCREEN" : ScreenReducer,
	"SIGNIN_SCREEN" : SigninScreenReducer,
	"REGISTER_SCREEN" : RegisterScreenReducer,
	"LIST_SCREEN" : ListScreenReducer,
	"MAP_SCREEN" : MapScreenReducer
});

const uiHandler = require("@juspay/mystique-backend").uiHandlers.android;
var dispatcher;

// Screens
const RootScreen = require("../views/RootScreen");
const SigninScreen = require("../views/SigninScreen");
const RegisterScreen = require("../views/RegisterScreen");
const ListScreen = require("../views/ListScreen");
const MapScreen = require("../views/MapScreen");

// ScreenActions
const RootScreenActions = require("../actions/RootScreenActions");
const SigninActions = require("../actions/SigninActions");
const RegisterScreenActions = require("../actions/RegisterScreenActions");
const ListScreenActions = require("../actions/ListActions");
const MapScreenActions = require("../actions/MapActions");


var determineScreen = (state, dispatcher) => {
	var screen;
	switch (state.global.currScreen) {
		case "SIGNIN_SCREEN" : 
		 		screen = new (SigninScreen(dispatcher, SigninActions))(null, null, state);
		 		break;
	 	case "REGISTER_SCREEN" : 
	 		console.log("REGISTER_SCREEN ");
	 		screen = new (RegisterScreen(dispatcher, RegisterScreenActions))(null, null, state);
	 		break; 
		case "LIST_SCREEN" : 
	 		console.log("LIST_SCREEN ");
	 		screen = new (ListScreen(dispatcher, ListScreenActions))(null, null, state);
	 		break; 
		case "MAP_SCREEN" : 
	 		console.log("MAP_SCREEN ");
	 		screen = new (MapScreen(dispatcher, MapScreenActions))(null, null, state);
	 		break; 
	 	default : 
	 		throw new Error("Current screen not handled "+ state.global.currScreen);
	 		break;
	}

	return screen;
}

var res;
var currView;
var CURRENT_SCREEN = null;

var Containers = {
	handleStateChange : (data) => {
		var state = data.state.global;
		console.log("STATE : " , data.state);
		if(state.currScreen) {
			console.log("SCREEN : ",state.currScreen);
			currView = (new (RootScreen(dispatcher, RootScreenActions))({}, determineScreen(data.state, dispatcher), data));
			return { render : currView.render()};
		} else {
			return currView.handleStateChange(data) || {};
		}
	}
}

dispatcher = require("@juspay/mystique-backend").stateManagers.dispatcher(Containers, uiHandler, reducer);

module.exports = {
	init : () => {
		dispatcher("SCREEN", "INIT_UI", {});
	}
}