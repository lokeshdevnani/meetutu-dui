module.exports = (dispatcher) => {
	return {
		showScreen : (action, payload) => {
			dispatcher("SCREEN", action, payload)
		}
	};
}