module.exports = (dispatcher) => {
	return {
		showScreen : (action, payload) => {
			//console.log("showScreen " ,action "Payload ",payload );
			dispatcher("SCREEN", action, payload)
		},
		proceed : (message) => {
			dispatcher("SIGNIN_SCREEN", "STORE_RESPONSE", {message : message});
		}
	};
}