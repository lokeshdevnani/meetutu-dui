module.exports = (dispatcher) => {
	return {
		showScreen : (action, payload) => {
			dispatcher("SCREEN", action, payload)
		},
		proceed : (message) => {
			dispatcher("SIGNIN_SCREEN", "STORE_RESPONSE", {message : message});
		}
	};
}