var firebase = require('firebase');
var config = {
	apiKey: "AIzaSyBt56U5rVvS3c-wrVpnRDvMthTMSfpi4Tg",
	authDomain: "meetutu-4c5b7.firebaseapp.com",
	databaseURL: "https://meetutu-4c5b7.firebaseio.com",
	storageBucket: "meetutu-4c5b7.appspot.com",
	messagingSenderId: "866845177898"
};
firebase.initializeApp(config);

var firebaseHelpers = {
    login: (credentials) => {
		console.log(credentials);
		return firebase.auth().signInWithEmailAndPassword(credentials.username, credentials.password);
	},
    register: (info) => {
        return firebase.auth().createUserWithEmailAndPassword(info.username, info.password);
    },
	updateUserSkills: (skills, skillsToLearn) => {
		var userId = firebase.auth().currentUser.uid;
		firebase.database().ref('/skills/' + userId).set({
			skills: skills,
			skillsToLearn: skillsToLearn
		});
	},
	getUserSkills: () => {
		var userId = firebase.auth().currentUser.uid;
		return firebase.database().ref('/skills/' + userId).once('value');
	}
};

module.exports = firebaseHelpers;
